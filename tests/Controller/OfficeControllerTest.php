<?php

namespace App\Test\Controller;

use App\Entity\Office;
use App\Repository\OfficeRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class OfficeControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private OfficeRepository $repository;
    private string $path = '/office/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->repository = static::getContainer()->get('doctrine')->getRepository(Office::class);

        foreach ($this->repository->findAll() as $object) {
            $this->repository->remove($object, true);
        }
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Office index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $originalNumObjectsInRepository = count($this->repository->findAll());

        $this->markTestIncomplete();
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'office[Office]' => 'Testing',
            'office[valid]' => 'Testing',
            'office[bookings]' => 'Testing',
            'office[caracteristics]' => 'Testing',
        ]);

        self::assertResponseRedirects('/office/');

        self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));
    }

    public function testShow(): void
    {
        $this->markTestIncomplete();
        $fixture = new Office();
        $fixture->setOffice('My Title');
        $fixture->setValid('My Title');
        $fixture->setBookings('My Title');
        $fixture->setCaracteristics('My Title');

        $this->repository->save($fixture, true);

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Office');

        // Use assertions to check that the properties are properly displayed.
    }

    public function testEdit(): void
    {
        $this->markTestIncomplete();
        $fixture = new Office();
        $fixture->setOffice('My Title');
        $fixture->setValid('My Title');
        $fixture->setBookings('My Title');
        $fixture->setCaracteristics('My Title');

        $this->repository->save($fixture, true);

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'office[Office]' => 'Something New',
            'office[valid]' => 'Something New',
            'office[bookings]' => 'Something New',
            'office[caracteristics]' => 'Something New',
        ]);

        self::assertResponseRedirects('/office/');

        $fixture = $this->repository->findAll();

        self::assertSame('Something New', $fixture[0]->getOffice());
        self::assertSame('Something New', $fixture[0]->getValid());
        self::assertSame('Something New', $fixture[0]->getBookings());
        self::assertSame('Something New', $fixture[0]->getCaracteristics());
    }

    public function testRemove(): void
    {
        $this->markTestIncomplete();

        $originalNumObjectsInRepository = count($this->repository->findAll());

        $fixture = new Office();
        $fixture->setOffice('My Title');
        $fixture->setValid('My Title');
        $fixture->setBookings('My Title');
        $fixture->setCaracteristics('My Title');

        $this->repository->save($fixture, true);

        self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertSame($originalNumObjectsInRepository, count($this->repository->findAll()));
        self::assertResponseRedirects('/office/');
    }
}
