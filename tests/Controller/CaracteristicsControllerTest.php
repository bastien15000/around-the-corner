<?php

namespace App\Test\Controller;

use App\Entity\Caracteristics;
use App\Repository\CaracteristicsRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CaracteristicsControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private CaracteristicsRepository $repository;
    private string $path = '/caracteristics/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->repository = static::getContainer()->get('doctrine')->getRepository(Caracteristics::class);

        foreach ($this->repository->findAll() as $object) {
            $this->repository->remove($object, true);
        }
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Caracteristic index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $originalNumObjectsInRepository = count($this->repository->findAll());

        $this->markTestIncomplete();
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'caracteristic[libelle]' => 'Testing',
            'caracteristic[type]' => 'Testing',
            'caracteristic[multiple]' => 'Testing',
            'caracteristic[office]' => 'Testing',
        ]);

        self::assertResponseRedirects('/caracteristics/');

        self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));
    }

    public function testShow(): void
    {
        $this->markTestIncomplete();
        $fixture = new Caracteristics();
        $fixture->setLibelle('My Title');
        $fixture->setType('My Title');
        $fixture->setMultiple('My Title');
        $fixture->setOffice('My Title');

        $this->repository->save($fixture, true);

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Caracteristic');

        // Use assertions to check that the properties are properly displayed.
    }

    public function testEdit(): void
    {
        $this->markTestIncomplete();
        $fixture = new Caracteristics();
        $fixture->setLibelle('My Title');
        $fixture->setType('My Title');
        $fixture->setMultiple('My Title');
        $fixture->setOffice('My Title');

        $this->repository->save($fixture, true);

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'caracteristic[libelle]' => 'Something New',
            'caracteristic[type]' => 'Something New',
            'caracteristic[multiple]' => 'Something New',
            'caracteristic[office]' => 'Something New',
        ]);

        self::assertResponseRedirects('/caracteristics/');

        $fixture = $this->repository->findAll();

        self::assertSame('Something New', $fixture[0]->getLibelle());
        self::assertSame('Something New', $fixture[0]->getType());
        self::assertSame('Something New', $fixture[0]->getMultiple());
        self::assertSame('Something New', $fixture[0]->getOffice());
    }

    public function testRemove(): void
    {
        $this->markTestIncomplete();

        $originalNumObjectsInRepository = count($this->repository->findAll());

        $fixture = new Caracteristics();
        $fixture->setLibelle('My Title');
        $fixture->setType('My Title');
        $fixture->setMultiple('My Title');
        $fixture->setOffice('My Title');

        $this->repository->save($fixture, true);

        self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertSame($originalNumObjectsInRepository, count($this->repository->findAll()));
        self::assertResponseRedirects('/caracteristics/');
    }
}
