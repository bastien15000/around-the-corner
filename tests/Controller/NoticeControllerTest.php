<?php

namespace App\Test\Controller;

use App\Entity\Notice;
use App\Repository\NoticeRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class NoticeControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private NoticeRepository $repository;
    private string $path = '/notice/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->repository = static::getContainer()->get('doctrine')->getRepository(Notice::class);

        foreach ($this->repository->findAll() as $object) {
            $this->repository->remove($object, true);
        }
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Notice index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $originalNumObjectsInRepository = count($this->repository->findAll());

        $this->markTestIncomplete();
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'notice[commentary]' => 'Testing',
            'notice[stars]' => 'Testing',
            'notice[office]' => 'Testing',
            'notice[user]' => 'Testing',
        ]);

        self::assertResponseRedirects('/notice/');

        self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));
    }

    public function testShow(): void
    {
        $this->markTestIncomplete();
        $fixture = new Notice();
        $fixture->setCommentary('My Title');
        $fixture->setStars('My Title');
        $fixture->setOffice('My Title');
        $fixture->setUser('My Title');

        $this->repository->save($fixture, true);

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Notice');

        // Use assertions to check that the properties are properly displayed.
    }

    public function testEdit(): void
    {
        $this->markTestIncomplete();
        $fixture = new Notice();
        $fixture->setCommentary('My Title');
        $fixture->setStars('My Title');
        $fixture->setOffice('My Title');
        $fixture->setUser('My Title');

        $this->repository->save($fixture, true);

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'notice[commentary]' => 'Something New',
            'notice[stars]' => 'Something New',
            'notice[office]' => 'Something New',
            'notice[user]' => 'Something New',
        ]);

        self::assertResponseRedirects('/notice/');

        $fixture = $this->repository->findAll();

        self::assertSame('Something New', $fixture[0]->getCommentary());
        self::assertSame('Something New', $fixture[0]->getStars());
        self::assertSame('Something New', $fixture[0]->getOffice());
        self::assertSame('Something New', $fixture[0]->getUser());
    }

    public function testRemove(): void
    {
        $this->markTestIncomplete();

        $originalNumObjectsInRepository = count($this->repository->findAll());

        $fixture = new Notice();
        $fixture->setCommentary('My Title');
        $fixture->setStars('My Title');
        $fixture->setOffice('My Title');
        $fixture->setUser('My Title');

        $this->repository->save($fixture, true);

        self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertSame($originalNumObjectsInRepository, count($this->repository->findAll()));
        self::assertResponseRedirects('/notice/');
    }
}
