/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import "./styles/app.scss";

// start the Stimulus application
import "./bootstrap";

const $ = require("jquery");
// this "modifies" the jquery module: adding behavior to it
// the bootstrap module doesn't export/return anything
// require("popper");
require("bootstrap");

import * as bootstrap from "bootstrap";
import { API } from "./js/class/API";
import like from "./js/utils/like";
import stars from "./js/utils/stars";

$(document).ready(() => {
  let tooltipTriggerList = [].slice.call($('[data-bs-toggle="tooltip"]'));
  let popoverTriggerList = [].slice.call($('[data-bs-toggle="popover"]'));
  tooltipTriggerList.map(
    (tooltipTriggerEl) => new bootstrap.Tooltip(tooltipTriggerEl)
  );
  popoverTriggerList.map(
    (popoverTriggerList) => new bootstrap.Popover(popoverTriggerList)
  );
});

global.api = new API();
