import { Calendar } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';


let modal = document.querySelector('#modalCalendar');


modal.addEventListener('shown.bs.modal', function() {
        var calendarEl = document.getElementById('calendar');
        const index = window.location.pathname.split("/").pop();
        if (calendarEl != undefined) {
            var calendar = new Calendar(calendarEl, {
                plugins: [ dayGridPlugin, timeGridPlugin, listPlugin ],
                initialView: 'dayGridMonth',
                headerToolbar: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'dayGridMonth,timeGridWeek,listWeek'
                },
                navLinks: true,
                selectable: true,
                selectMirror: true,
                select: function(info) {
                    console.log('selected ' + info.startStr + ' to ' + info.endStr);
                },
                eventClick: function(info) {
                    let input_startDate = document.querySelector('input[name=start_date]');
                    let input_endDate = document.querySelector('input[name=end_date]');
                    let startUi = document.querySelector('#start_ui');
                    let endUi = document.querySelector('#end_ui');

                    let dateStartShow = new Date(info.event.start);
                    let dateEndShow = new Date(info.event.end);
                    startUi.innerHTML = `<h3>Départ</h3><p>${dateStartShow.getDate()}/${(dateStartShow.getMonth() + 1)}/${dateStartShow.getFullYear()}</p>`;
                    endUi.innerHTML = `<h3>Arrivée</h3><p>${dateEndShow.getDate()}/${(dateEndShow.getMonth() + 1)}/${dateEndShow.getFullYear()}</p>`;

                    input_startDate.value = dateStartShow.getTime();
                    input_endDate.value = dateEndShow.getTime();
                    
                    $('#modalCalendar').modal('hide');
                },
                events: `/office/calendar/${index}`
            });
            calendar.render();
        }

        console.log();
})

