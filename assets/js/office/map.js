document.addEventListener("DOMContentLoaded", function () {
  var markerPositions;
  var mymap = L.map("mapid").setView([45.75, 4.85], 9);

  L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
    attribution: "Map data © OpenStreetMap contributors",
    maxZoom: 19,
  }).addTo(mymap);

  var Icon = L.icon({
    iconUrl: "/img/Local1.svg",

    iconSize: [50, 75], // size of the icon
    iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62], // the same for the shadow
    popupAnchor: [-3, -76], // point from which the popup should open relative to the iconAnchor
  });

  fetch(
    "/office/coordinate" +
      (typeof idOffice !== "undefined" ? "/" + idOffice : "")
  )
    .then((response) => response.json())
    .then((data) => {
      markerPositions = data.coordinates;
      infos = data.infos;
      for (var i = 0; i < markerPositions.length; i++) {
        var marker = L.marker(markerPositions[i], { icon: Icon }).addTo(mymap);
        info = infos[i];
        marker.on("click", function (event) {
          this.bindPopup(
            "<b>" +
              info.nom +
              "</b><br><p>" +
              info.addresse +
              "</p><p>" +
              info.taille +
              "m² pour " +
              info.capacite +
              " personnes au prix de " +
              info.prix +
              "€</p>"
          ).openPopup();
          this.off("click");
          this.on("click", function (event) {
            this.bindPopup(
              "<b>" +
                info.nom +
                "</b><br><p>" +
                info.addresse +
                "</p><p>" +
                info.taille +
                "m² pour " +
                info.capacite +
                " personnes au prix de " +
                info.prix +
                "€</p>"
            ).openPopup();
          });
        });
      }
    });
});
