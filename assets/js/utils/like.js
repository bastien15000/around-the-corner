document.querySelectorAll(".fa-heart").forEach((icon) => {
  icon.addEventListener("click", () => {
    icon.classList.toggle("fa-regular");
    icon.classList.toggle("fa-solid");
  });
});
