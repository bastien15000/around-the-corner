$('#updateUserProfilePicture').click(() => {
    let newProfilePictureUrl = $('#profilePictureUrl').val() || $('#profilePictureUrl').val() === '' && '/img/unknown.png';
    $('#profilePictureUrlPreview').attr('src', newProfilePictureUrl);
});