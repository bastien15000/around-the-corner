$('.deleteOffice').click((ev)=> {
    api.deleteOffice(ev.target.dataset.officeid)
        .then(() => window.location.reload())
        .catch((err) => console.log(err))
})