import { Office } from "../class/Office";

function updateModalInformation(data) {

  $('.update-profile-title h2').text("Modifier le bureau");

  $('#addOfficeBtn').hide();
  $('#updateOfficeBtn').show();

  $("#updateOfficeBtn").attr('data-officeId', data.id);

  $('#office-name').val(data.name);
  $('#office-address').val(data.adress);
  $('#office-city').val(data.city);
  $('#office-price').val(data.price);
  $('#office-places').val(data.capacity);

  let allInputCarac = $('.office-form-caracteristics input[type="checkbox"]');
  let allInputDispo = $('.office-form-dispo input[type="checkbox"]');
  let officeCaracId = [];

  data.caracteristics.forEach((c) => {
    officeCaracId.push(c.id);
  });


  for (let i = 0; i < allInputCarac.length; i++) {
    allInputCarac[i].checked = officeCaracId.includes(parseInt(allInputCarac[i].value));
  }

  for (let i = 0; i < allInputDispo.length; i++) {
    allInputDispo[i].checked = data.availables_day.includes(allInputDispo[i].id + '_morning') ||
      data.availables_day.includes(allInputDispo[i].id + '_afternoon');
  }
}

$(".triggerUpdateOfficeModal").click((ev) => {
  api
      .getOfficeById(ev.target.value)
      .then((data) => updateModalInformation(data))
      .catch((err) => console.log(err));
});

$("#updateOfficeBtn").click((ev) => {
  let officeInformations = new Office();

  officeInformations.id = ev.target.dataset.officeid;
  officeInformations.name = $('#office-name').val();
  officeInformations.address = $("#office-address").val();
  officeInformations.city= $("#office-city").val();
  officeInformations.price = $("#office-price").val();
  officeInformations.places = $("#office-places").val();

  let allCheckedCarac = $('.office-form-caracteristics input[type="checkbox"]:checked');
  let allCheckedDispo = $('.office-form-dispo input[type="checkbox"]:checked');

  for (let i = 0; i < allCheckedCarac.length; i++) {
    officeInformations.caracteristics.push(allCheckedCarac[i].value);
  }

  for (let i = 0; i < allCheckedDispo.length; i++) {
    officeInformations.availables_day.push(allCheckedDispo[i].id);
  }

  api
    .updateOfficeProfile(officeInformations)
    .then(() => window.location.reload())
    .catch((err) => console.log(err));
});
