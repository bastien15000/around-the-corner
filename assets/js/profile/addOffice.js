import {Office} from "../class/Office";

$("#triggerAddOfficeModal").click(() => {
    $('.update-profile-title h2').text("Ajouter le bureau");

    $('#updateOfficeBtn').hide()
    $('#addOfficeBtn').show();

    $("#updateOfficeBtn").attr('data-officeId', "");

    $('#office-name').val("");
    $('#office-address').val("");
    $('#office-city').val("");
    $('#office-price').val("");
    $('#office-places').val("");

    let allInputCarac = $('.office-form-caracteristics input[type="checkbox"]');
    let allInputDispo = $('.office-form-dispo input[type="checkbox"]');

    for (let i = 0; i < allInputCarac.length; i++) {
        allInputCarac[i].checked = false;
    }

    for (let i = 0; i < allInputDispo.length; i++) {
        allInputDispo[i].checked = false;
    }
});

$("#addOfficeBtn").click(() => {
    let officeInformations = new Office();

    officeInformations.id = -1;
    officeInformations.name = $('#office-name').val();
    officeInformations.address = $("#office-address").val();
    officeInformations.city= $("#office-city").val();
    officeInformations.price = $("#office-price").val();
    officeInformations.places = $("#office-places").val();

    let allCheckedCarac = $('.office-form-caracteristics input[type="checkbox"]:checked');
    let allCheckedDispo = $('.office-form-dispo input[type="checkbox"]:checked');

    for (let i = 0; i < allCheckedCarac.length; i++) {
        officeInformations.caracteristics.push(allCheckedCarac[i].value);
    }

    for (let i = 0; i < allCheckedDispo.length; i++) {
        officeInformations.availables_day.push(allCheckedDispo[i].id);
    }

    api
        .addOfficeProfile(officeInformations)
        .then(() => window.location.reload())
        .catch((err) => console.log(err));
});