import {User} from "../class/User";

$('#updateProfileBtn').click((ev) => {
    let userInformations = new User();

    userInformations.id = ev.target.dataset.userid;
    userInformations.firstname = $('#user-firstname').val();
    userInformations.lastname = $('#user-lastname').val();
    userInformations.address = $('#user-address').val();
    userInformations.city = $('#user-city').val();
    userInformations.email = $('#user-email').val();
    userInformations.profilePictureUrl = $('#profilePictureUrl').val();
    userInformations.phone = $('#user-phone').val();

    api.updateUserProfile(userInformations)
        .then(() => window.location.reload())
        .catch((err) => console.log(err));
})