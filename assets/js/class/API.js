export class API {

  getOfficeById(officeId) {
    return this.get(`/office/api/${officeId}`);
  }

  updateUserProfile(user) {
    const formData = this.getFormData([
      {
        key: "user",
        value: JSON.stringify(user),
      },
    ]);

    return this.post(`/user/api/${user.id}/edit`, formData);
  }

  updateOfficeProfile(office) {
    const formData = this.getFormData([
      {
        key: "office",
        value: JSON.stringify(office),
      },
    ]);

    return this.post(`/office/api/${office.id}/edit`, formData);
  }

  addOfficeProfile(office) {
    const formData = this.getFormData([
      {
        key: "office",
        value: JSON.stringify(office),
      },
    ]);

    return this.post(`/office/api`, formData);
  }

  deleteOffice(officeid) {

    return this.post(`/office/api/${officeid}/delete`);
  }

  async get(url) {
    const response = await fetch(url, {
      method: "GET",
    });

    return await response.json();
  }

  async post(url, params) {
    const response = await fetch(url, {
      method: "POST",
      body: params,
    });

    return await response.json();
  }

  getFormData(data) {
    const formData = new FormData();
    data.forEach((datum) => {
      formData.append(datum.key, datum.value);
    });

    return formData;
  }
}
