export class User {

    id = null;

    email = null;

    firstname = null;

    lastname = null;

    profilePictureURL = null;

    address = null;

    city = null;

    phone = null;
}