import { Calendar } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';


document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');
    var calendar = new Calendar(calendarEl, {
        plugins: [ dayGridPlugin, timeGridPlugin, listPlugin ],
        initialView: 'dayGridMonth',
        headerToolbar: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,listWeek'
        },
        navLinks: true,
        selectable: true,
        selectMirror: true,
        select: function(info) {
            console.log('selected ' + info.startStr + ' to ' + info.endStr);
        },
        eventClick: function(info) {
            console.log('event clicked');
            // Ouverture de la modal
        },
        events: "{{ path('calendar_events') }}"
    });
    calendar.render();
});

console.log("test");