<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230327130949 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE booking (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, state SMALLINT NOT NULL, created_date DATETIME NOT NULL, updated_date DATETIME DEFAULT NULL, INDEX IDX_E00CEDDEA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE booking_office (booking_id INT NOT NULL, office_id INT NOT NULL, INDEX IDX_CFCB50FC3301C60 (booking_id), INDEX IDX_CFCB50FCFFA0C224 (office_id), PRIMARY KEY(booking_id, office_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE caracteristics (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, multiple SMALLINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE caracteristics_office (caracteristics_id INT NOT NULL, office_id INT NOT NULL, INDEX IDX_A8A7B76C5D20926C (caracteristics_id), INDEX IDX_A8A7B76CFFA0C224 (office_id), PRIMARY KEY(caracteristics_id, office_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE notice (id INT AUTO_INCREMENT NOT NULL, office_id INT DEFAULT NULL, user_id INT DEFAULT NULL, commentary VARCHAR(255) NOT NULL, stars SMALLINT DEFAULT NULL, INDEX IDX_480D45C2FFA0C224 (office_id), INDEX IDX_480D45C2A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE office (id INT AUTO_INCREMENT NOT NULL, office VARCHAR(255) NOT NULL, valid SMALLINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE period (id INT AUTO_INCREMENT NOT NULL, date_start DATETIME NOT NULL, date_end DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, firstname VARCHAR(100) DEFAULT NULL, lastname VARCHAR(100) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL, available_at DATETIME NOT NULL, delivered_at DATETIME DEFAULT NULL, INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDEA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE booking_office ADD CONSTRAINT FK_CFCB50FC3301C60 FOREIGN KEY (booking_id) REFERENCES booking (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE booking_office ADD CONSTRAINT FK_CFCB50FCFFA0C224 FOREIGN KEY (office_id) REFERENCES office (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE caracteristics_office ADD CONSTRAINT FK_A8A7B76C5D20926C FOREIGN KEY (caracteristics_id) REFERENCES caracteristics (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE caracteristics_office ADD CONSTRAINT FK_A8A7B76CFFA0C224 FOREIGN KEY (office_id) REFERENCES office (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE notice ADD CONSTRAINT FK_480D45C2FFA0C224 FOREIGN KEY (office_id) REFERENCES office (id)');
        $this->addSql('ALTER TABLE notice ADD CONSTRAINT FK_480D45C2A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDEA76ED395');
        $this->addSql('ALTER TABLE booking_office DROP FOREIGN KEY FK_CFCB50FC3301C60');
        $this->addSql('ALTER TABLE booking_office DROP FOREIGN KEY FK_CFCB50FCFFA0C224');
        $this->addSql('ALTER TABLE caracteristics_office DROP FOREIGN KEY FK_A8A7B76C5D20926C');
        $this->addSql('ALTER TABLE caracteristics_office DROP FOREIGN KEY FK_A8A7B76CFFA0C224');
        $this->addSql('ALTER TABLE notice DROP FOREIGN KEY FK_480D45C2FFA0C224');
        $this->addSql('ALTER TABLE notice DROP FOREIGN KEY FK_480D45C2A76ED395');
        $this->addSql('DROP TABLE booking');
        $this->addSql('DROP TABLE booking_office');
        $this->addSql('DROP TABLE caracteristics');
        $this->addSql('DROP TABLE caracteristics_office');
        $this->addSql('DROP TABLE notice');
        $this->addSql('DROP TABLE office');
        $this->addSql('DROP TABLE period');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
