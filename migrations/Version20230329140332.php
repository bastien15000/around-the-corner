<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230329140332 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE period DROP FOREIGN KEY FK_C5B81ECEFFA0C224');
        $this->addSql('DROP INDEX IDX_C5B81ECEFFA0C224 ON period');
        $this->addSql('DROP TABLE period');

    }

    public function down(Schema $schema): void
    {
    }
}
