<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230328130010 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("INSERT INTO `caracteristics` (`id`, `libelle`, `type`, `multiple`) VALUES
            (1, 'Equipé (écran, clavier, ...)', 'boolean', 0),
            (2, 'Wifi', 'boolean', 0),
            (3, 'Accès à une cuisine', 'boolean', 0),
            (4, 'Salle de repos', 'boolean', 0),
            (5, 'Connexion éthernet', 'boolean', 0),
            (6, 'Présence de résidents', 'boolean', 0),
            (7, 'Climatisation', 'boolean', 0),
            (8, 'Calme', 'boolean', 0)");
    }

    public function down(Schema $schema): void
    {
        $this->addSql('TRUNCATE TABLE `aroundcorner`.`caracteristics`');

    }
}
