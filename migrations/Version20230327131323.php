<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230327131323 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE period ADD office_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE period ADD CONSTRAINT FK_C5B81ECEFFA0C224 FOREIGN KEY (office_id) REFERENCES booking (id)');
        $this->addSql('CREATE INDEX IDX_C5B81ECEFFA0C224 ON period (office_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE period DROP FOREIGN KEY FK_C5B81ECEFFA0C224');
        $this->addSql('DROP INDEX IDX_C5B81ECEFFA0C224 ON period');
        $this->addSql('ALTER TABLE period DROP office_id');
    }
}
