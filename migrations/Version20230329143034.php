<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230329143034 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE booking_office DROP FOREIGN KEY FK_CFCB50FC3301C60');
        $this->addSql('ALTER TABLE booking_office DROP FOREIGN KEY FK_CFCB50FCFFA0C224');
        $this->addSql('DROP TABLE booking_office');
        $this->addSql('ALTER TABLE booking ADD office_id INT NOT NULL, ADD start_date DATETIME NOT NULL, ADD end_date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDEFFA0C224 FOREIGN KEY (office_id) REFERENCES office (id)');
        $this->addSql('CREATE INDEX IDX_E00CEDDEFFA0C224 ON booking (office_id)');
        $this->addSql('ALTER TABLE office ADD description VARCHAR(500) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE booking_office (booking_id INT NOT NULL, office_id INT NOT NULL, INDEX IDX_CFCB50FCFFA0C224 (office_id), INDEX IDX_CFCB50FC3301C60 (booking_id), PRIMARY KEY(booking_id, office_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE booking_office ADD CONSTRAINT FK_CFCB50FC3301C60 FOREIGN KEY (booking_id) REFERENCES booking (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE booking_office ADD CONSTRAINT FK_CFCB50FCFFA0C224 FOREIGN KEY (office_id) REFERENCES office (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE booking DROP FOREIGN KEY FK_E00CEDDEFFA0C224');
        $this->addSql('DROP INDEX IDX_E00CEDDEFFA0C224 ON booking');
        $this->addSql('ALTER TABLE booking DROP office_id, DROP start_date, DROP end_date');
        $this->addSql('ALTER TABLE office DROP description');
    }
}
