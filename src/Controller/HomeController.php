<?php

namespace App\Controller;

use App\Repository\CaracteristicsRepository;
use App\Repository\OfficeRepository;
use App\Service\OfficeService;
use App\Service\PaginationData;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{

    public function __construct(
        private OfficeService $officeService,
        private CaracteristicsRepository $caracteristicsRepository
    ) {}

    #[Route('/', name: 'app_home')]
    public function index(PaginationData $paginationData, Request $request): Response
    {

        return $this->render('home/index.html.twig', [
            'caracteristics' => $this->caracteristicsRepository->findAll(),
            'offices' => $paginationData->getPaginationData($request, $this->officeService->search($request->getContent()))
        ]);
    }
}
