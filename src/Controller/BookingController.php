<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Entity\Office;
use App\Form\BookingType;
use App\Repository\BookingRepository;
use App\Repository\OfficeRepository;
use App\Repository\UserRepository;
use App\Service\AppSerializer;
use DateTime;
use App\Service\PdfService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Security\Core\Security;

#[Route('/booking')]
class BookingController extends AbstractController
{
    private AppSerializer $appSerializer;

    public function __construct()
    {
        $this->appSerializer = new AppSerializer();
    }

    #[Route('/', name: 'app_booking_index', methods: ['GET'])]
    public function index(BookingRepository $bookingRepository): Response
    {
        return $this->render('booking/index.html.twig', [
            'bookings' => $bookingRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_booking_new', methods: ['GET', 'POST'])]
    public function new(Request $request, BookingRepository $bookingRepository, OfficeRepository $officeRepository, Security $security): Response
    {
        $booking = new Booking();
        $form = $this->createForm(BookingType::class, $booking);

        

        if (!empty($request->getContent())) {
            $startDate = $request->get('start_date');
            $endDate = $request->get('end_date');
            $capacity = $request->get('capacity');
            $officeId = $request->get('office');
            $user = $security->getUser();
                
            if (!empty($officeId)) {
                $office = $officeRepository->findOneBy(["id" => $officeId]);

                if (!empty($startDate) && !empty($endDate) && !empty($capacity) && !empty($office) && !empty($user)) {

                    $booking->setStartDate(new DateTime(date('Y-m-d H:i:s',$startDate)))
                    ->setEndDate(new DateTime(date('Y-m-d H:i:s',$endDate)))
                    ->setCapacity($capacity)
                    ->setState(3)
                    ->setCreatedDate(new DateTime())
                    ->setOffice($office)
                    ->setUser($user);
    
                    $bookingRepository->save($booking, true);
    
                    return $this->redirectToRoute('app_home');
                } 
            }
            
            if(!empty($officeId)) {
                return $this->redirectToRoute('app_office_show',  array('id' => $officeId));
            } else {
                return $this->redirectToRoute('app_home');
            }
           
        }


        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $bookingRepository->save($booking, true);

            return $this->redirectToRoute('app_booking_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('booking/new.html.twig', [
            'booking' => $booking,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_booking_show', methods: ['GET'])]
    public function show(Booking $booking): Response
    {
        return $this->render('booking/show.html.twig', [
            'booking' => $booking,
        ]);
    }

    #[Route('/cancel/{id}', name: 'app_booking_cancel', methods: ['GET'])]
    public function cancelReservation(Booking $booking, BookingRepository $bookingRepository): Response
    {
        if (!empty($booking)) {
            $booking->setState(4);
            $bookingRepository->save($booking, true);
        }

        return $this->redirect('/user/profile');
    }

    #[Route('/{id}/edit', name: 'app_booking_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Booking $booking, BookingRepository $bookingRepository): Response
    {
        $form = $this->createForm(BookingType::class, $booking);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $bookingRepository->save($booking, true);

            return $this->redirectToRoute('app_booking_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('booking/edit.html.twig', [
            'booking' => $booking,
            'form' => $form,
        ]);
    }

    #[Route('/delete/{id}', name: 'app_booking_delete', methods: ['GET','POST'])]
    public function delete(Request $request, Booking $booking, BookingRepository $bookingRepository): Response
    {        
        if ($this->isCsrfTokenValid('delete'.$booking->getId(), $request->request->get('_token'))) {
            $bookingRepository->remove($booking, true);
        }

        return $this->redirectToRoute('app_booking_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/contrat/{id}', name: "app_booking_contrat", methods: ['GET'])]
    public function contratPdf(PdfService $pdf, Request $request, Booking $booking, BookingRepository $bookingRepository) : Response
    {
        $html = $this->renderView('booking/contrat.html.twig', ['booking' => $booking]);
        return $pdf->generateBinaryPDF($html, 'contrat');
    }

    #[Route('/facture/{id}', name:"app_booking_facture", methods: ['GET'])]
    public function facturePdf(PdfService $pdf, Request $request, Booking $booking, BookingRepository $bookingRepository, OfficeRepository $officeRepository, UserRepository $userRepository) : Response
    {
        $office = (object)$officeRepository->findOneBy(['id' => $booking->getOffice()->getId()]);
        $user = $userRepository->findOneBy(['id' => $booking->getUser()->getId()]);
        $html = $this->renderView('booking/facture.html.twig', ['booking' => $booking, 'office' => $office, 'user' => $user]);
        return $pdf->generateBinaryPDF($html, 'facture');
    }
}
