<?php

namespace App\Controller;

use App\Entity\Caracteristics;
use App\Form\CaracteristicsType;
use App\Repository\CaracteristicsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/caracteristics')]
class CaracteristicsController extends AbstractController
{
    #[Route('/', name: 'app_caracteristics_index', methods: ['GET'])]
    public function index(CaracteristicsRepository $caracteristicsRepository): Response
    {
        return $this->render('caracteristics/index.html.twig', [
            'caracteristics' => $caracteristicsRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_caracteristics_new', methods: ['GET', 'POST'])]
    public function new(Request $request, CaracteristicsRepository $caracteristicsRepository): Response
    {
        $caracteristic = new Caracteristics();
        $form = $this->createForm(CaracteristicsType::class, $caracteristic);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $caracteristicsRepository->save($caracteristic, true);

            return $this->redirectToRoute('app_caracteristics_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('caracteristics/new.html.twig', [
            'caracteristic' => $caracteristic,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_caracteristics_show', methods: ['GET'])]
    public function show(Caracteristics $caracteristic): Response
    {
        return $this->render('caracteristics/show.html.twig', [
            'caracteristic' => $caracteristic,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_caracteristics_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Caracteristics $caracteristic, CaracteristicsRepository $caracteristicsRepository): Response
    {
        $form = $this->createForm(CaracteristicsType::class, $caracteristic);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $caracteristicsRepository->save($caracteristic, true);

            return $this->redirectToRoute('app_caracteristics_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('caracteristics/edit.html.twig', [
            'caracteristic' => $caracteristic,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_caracteristics_delete', methods: ['POST'])]
    public function delete(Request $request, Caracteristics $caracteristic, CaracteristicsRepository $caracteristicsRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$caracteristic->getId(), $request->request->get('_token'))) {
            $caracteristicsRepository->remove($caracteristic, true);
        }

        return $this->redirectToRoute('app_caracteristics_index', [], Response::HTTP_SEE_OTHER);
    }
}
