<?php

namespace App\Controller;

use App\Entity\Office;
use App\Form\OfficeType;
use App\Repository\BookingRepository;
use App\Repository\OfficeRepository;
use App\Service\AppSerializer;
use App\Service\CalenderService;
use App\Service\OfficeService;
use App\Repository\CaracteristicsRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/office')]
class OfficeController extends AbstractController
{

    public function __construct(
        private OfficeService $officeService,
        private OfficeRepository $officeRepository
    ) {}

    #[Route('/', name: 'app_office_index', methods: ['GET', 'POST'])]
    public function index(): Response
    {
        return $this->render('office/index.html.twig', [
            'offices' => $this->officeRepository->findAll(),
        ]);
    }

    #[Route('/map', name: 'app_office_map', methods: ['GET'])]
    public function map(): Response
    {
        return $this->render('/office/map.html.twig');
    }

    #[Route('/search', name: 'app_office_search', methods: ['POST'])]
    public function search(Request $request, OfficeService $officeService, AppSerializer $appSerializer)
    {
        return new Response(serialize($officeService->search($request)));
    }

    #[Route('/calendar/{id}', name: 'calendar_events', methods: ['GET'])]
    public function getCalendar(Request $request, Office $office, CalenderService $calenderService, BookingRepository $bookingRepository)
    {

        $availables_days = $office->getAvailablesDay();

        $calenderStartDate = new DateTime($_GET["start"]);
        $calenderEndDate = new DateTime($_GET["end"]);

        $bookings = $bookingRepository->findBy(['office' => $office]);

        if (!empty($availables_days) && !empty($calenderStartDate) && !empty($calenderEndDate) && !empty($office->getName())) {
            $events = $calenderService->getEvents($availables_days, $calenderStartDate, $calenderEndDate, $office->getName());
            if (!empty($bookings)) {
                $events = $calenderService->filter($events, $bookings);
            }
            return $this->json($events);
        }

        return $this->json([]);
    }

    #[Route('/coordinate/{id<\d+>?}', name: 'app_office_coordinate', methods: ['GET'])]
    public function coordinate(?Office $office): JsonResponse
    {
        $coordinates = [];
        $infos = [];
        $offices = !empty($office) ? array($office) : $this->officeRepository->findAll();
        foreach ($offices as $office) {
            if (!empty($office->getLatitude()) && !empty($office->getLongitude())) {
                $coordinates[] = [floatval($office->getLatitude()), floatval($office->getLongitude())];
                $infos[] = ['nom' => $office->getName(), 'addresse' => $office->getAdress(), 'prix' => $office->getPrice(), 'taille' => $office->getSize(), 'capacite' => $office->getCapacity()];
            }
        }
        return $this->json(['coordinates' => $coordinates, 'infos' => $infos]);
    }

    #[Route('/{id}', name: 'app_office_show', methods: ['GET'])]
    public function show(Office $office, CaracteristicsRepository $caracteristicRepository): Response
    {
        return $this->render('office/show.html.twig', [
            'office' => $office,
            'caracteristics' => $caracteristicRepository->findAll(),
        ]);
    }

    #[Route('/{id}/edit', name: 'app_office_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Office $office): Response
    {
        $form = $this->createForm(OfficeType::class, $office);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->officeRepository->save($office, true);

            return $this->redirectToRoute('app_office_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('office/edit.html.twig', [
            'office' => $office,
            'form' => $form,
        ]);
    }

    #[Route('/api/{id}/edit', name: 'api_office_edit', methods: ['POST'])]
    public function updateOffice(Request $request): Response
    {
        $officeInformations = json_decode($request->request->get('office'));

        $office = $this->officeService->updateOffice($officeInformations);

        return $this->json($office, 200, [], ['groups' => 'classicalOffice']);
    }

    #[Route('/api', name: 'api_office_add', methods: ['POST'])]
    public function addOffice(Request $request, AppSerializer $appSerializer): Response
    {
        $officeInformations = json_decode($request->request->get('office'));

        $status = $this->officeService->create($officeInformations, $this->getUser());

        return $appSerializer->serialize($status['message']);
    }

    #[Route('/api/{id}', name: 'api_office_get', methods: ['GET'])]
    public function getOffice(Request $request): Response
    {
        return $this->json($this->officeRepository->findOneBy(['id' => $request->get('id')]), 200, [], ['groups' => 'classicalUser']);
    }


    #[Route('/{id}', name: 'app_office_delete', methods: ['POST'])]
    public function delete(Request $request, Office $office): Response
    {
        if ($this->isCsrfTokenValid('delete' . $office->getId(), $request->request->get('_token'))) {
            $this->officeRepository->remove($office, true);
        }

        return $this->redirectToRoute('app_office_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/api/{id}/delete', name: 'api_office_delete', methods: ['POST'])]
    public function deleteApi(Office $office, AppSerializer $appSerializer): Response
    {
        $this->officeRepository->remove($office, true);

        return $appSerializer->serialize("Office deleted");
    }
}
