<?php

namespace App\Service;

use Knp\Component\Pager\PaginatorInterface;
use App\Repository\OfficeRepository;
use Symfony\Component\HttpFoundation\Request;

class PaginationData
{
    private PaginatorInterface $paginator;
    private OfficeRepository $officeRepository;
    private Request $request;

    public function __construct(PaginatorInterface $paginator, OfficeRepository $officeRepository)
    {
        $this->paginator = $paginator;
        $this->officeRepository = $officeRepository;
    }

    public function getPaginationData(Request $request, $data)
    {
        $paginationData = $this->paginator->paginate(
            $data, // Requête contenant les données à paginer (ici nos offices)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            5 // Nombre de résultats par page
        );

        return $paginationData;
    }
}
