<?php

namespace App\Service;

use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Component\HttpFoundation\Response;

class PdfService
{
    private $domPdf;

    public function  __construct()
    {
        $this->domPdf = new DomPdf();
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Garamond');
        $pdfOptions->set('isRemoteEnabled', true);
        $this->domPdf->setOptions($pdfOptions);
    }

    public function generateBinaryPDF($html, $filename)
    {
        $pdfGenerator = new \Dompdf\Dompdf();
        $pdfGenerator->loadHtml($html);
        $pdfGenerator->setPaper('A4', 'portrait');

        $pdfGenerator->render();
        $pdfContent = $pdfGenerator->output();
        
        return new Response($pdfContent, 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="' . $filename . '.pdf"'
        ]);
    }
}
