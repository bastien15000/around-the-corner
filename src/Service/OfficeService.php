<?php

namespace App\Service;

use App\Entity\Office;
use App\Entity\User;
use App\Repository\CaracteristicsRepository;
use App\Repository\OfficeRepository;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Security\Core\User\UserInterface;

class OfficeService
{

    public function __construct(
        private CaracteristicsRepository $caracteristicsRepository,
        private OfficeRepository $officeRepository
    ) {}

    public function create(object $newOfficeInformations, UserInterface $user): array
    {
        try {
            $office = new Office();

            $availables_day = [];

            foreach($newOfficeInformations->availables_day as $availableDayDto) {
                $availables_day[] = $availableDayDto . "_morning";
                $availables_day[] = $availableDayDto . "_afternoon";
            }

            $office->setName($newOfficeInformations->name);
            $office->setAdress($newOfficeInformations->address);
            $office->setCity($newOfficeInformations->city);
            $office->setPrice($newOfficeInformations->price);
            $office->setCapacity($newOfficeInformations->places);
            $office->setSize('12');
            $office->setPictureUrl("https://media.istockphoto.com/id/1177797403/fr/photo/immeubles-dappartements-modernes-sur-une-journ%C3%A9e-ensoleill%C3%A9e-avec-un-ciel-bleu.jpg?s=612x612&w=0&k=20&c=RYVqzUo-p9hfMYyHIQyicLMgO84SjLZe70j_CNXaAI0=");

            $office->setAvailablesDay($availables_day);
            $office->setValid(1);

            foreach ($newOfficeInformations->caracteristics as $caracteristicId) {
                $caracteristic = $this->caracteristicsRepository->findOneBy(["id" => $caracteristicId]);
                $office->addCaracteristic($caracteristic);
            }

            $httpClient = HttpClient::create();
            $response = $httpClient->request('GET', 'https://api-adresse.data.gouv.fr/search/?q=' . $office->getAdress() . ' ' . $office->getCity() ?? '');
            $gpsData = json_decode($response->getContent(), true);
            $office->setLatitude($gpsData['features'][0]['geometry']['coordinates'][1])
                ->setLongitude($gpsData['features'][0]['geometry']['coordinates'][0]);

            $user->addOffice($office);

            $this->officeRepository->save($office, true);

            return ['code' => 200, 'message' => 'Office created'];
        } catch(\Exception $e) {
            return ['code' => 500, 'message' => $e->getMessage()];
        }
    }

    public function updateOffice(object $newOfficeInformations): Office {

        $currentOffice = $this->officeRepository->findOneBy(['id' => $newOfficeInformations->id]);

        $currentOffice->setName($newOfficeInformations->name);
        $currentOffice->setAdress($newOfficeInformations->address);
        $currentOffice->setCity($newOfficeInformations->city);
        $currentOffice->setPrice($newOfficeInformations->price);
        $currentOffice->setCapacity($newOfficeInformations->places);

        $currentOffice->removeCaracteristics();

        foreach ($newOfficeInformations->caracteristics as $caracteristicId) {
            $caracteristic = $this->caracteristicsRepository->findOneBy(["id" => $caracteristicId]);
            $currentOffice->addCaracteristic($caracteristic);
        }

        $availables_day = [];

        foreach($newOfficeInformations->availables_day as $availableDayDto) {
            $availables_day[] = $availableDayDto . "_morning";
            $availables_day[] = $availableDayDto . "_afternoon";
        }

        $currentOffice->setAvailablesDay($availables_day);

        $this->officeRepository->save($currentOffice, true);

        return $currentOffice;
    }

    public function search(string $data)
    {
        return $this->officeRepository->search($data ?? []);
    }
}
