<?php 
namespace App\Service;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;

class AppSerializer {

    private $encode;
    private $normalizers;
    private Serializer $serializer;
    private $context;
    
    public function __construct() {
        $this->encode = [new JsonEncoder(), new JsonEncoder()];
        $this->normalizers = [new ObjectNormalizer()];
        $this->serializer = new Serializer($this->normalizers, $this->encode);
        $this->context = [ AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function($object, $format, $context) {
                return $object->getId();
            },
            AbstractObjectNormalizer::PRESERVE_EMPTY_OBJECTS => true
        ];
    }

    public function serialize($object, $error=""): Response {
        if (!empty($object)) {
            $response = ['response'=>$object];

            if (!empty($error)) {
                $response['error'] = $error;
            }
                    
            return new Response($this->serializer->serialize($response, 'json', $this->context));

        } else {
            return new Response($this->serializer->serialize(['errror'=> "Object can't be serialize"], 'json'));
        }
    }

    public function deserialize($data, String $type, String $format) {
        return $this->serializer->deserialize($data, $type, $format);
    }
}


