<?php

namespace App\Service;

use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class UserService
{

    public function __construct(
        private UserRepository $userRepository,
        private Security $security
    ) {}

    public function updateUserProfile(object $newUserInformation): UserInterface {
        $user = $this->security->getUser();
        $user->setFirstName($newUserInformation->firstname);
        $user->setLastName($newUserInformation->lastname);
        $user->setAddress($newUserInformation->address);
        $user->setCity($newUserInformation->city);
        $user->setEmail($newUserInformation->email);
        $user->setProfilePictureUrl($newUserInformation->profilePictureUrl);
        $user->setPhone($newUserInformation->phone);

        $this->userRepository->save($user, true);

        return $user;
    }
}