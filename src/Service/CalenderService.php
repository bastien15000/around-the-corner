<?php
namespace App\Service;

use DateTime;

class CalenderService {

    public function getEvents(array $availables_days, DateTime $calenderStartDate, DateTime $calenderEndDate, String $officeName) {
        $events = [];

        while ($calenderStartDate < $calenderEndDate) {            
            switch (date('D', strtotime($calenderStartDate->format('Y-m-d H:i:s')))) {
                case 'Mon':
                    $events =  CalenderService::generateEventDay('monday', $availables_days, $officeName, $calenderStartDate, $events);
                    break;
                case 'Tue':  
                    $events = CalenderService::generateEventDay('tuesday', $availables_days, $officeName, $calenderStartDate, $events);              
                    break;
                case 'Wed':
                    $events = CalenderService::generateEventDay('wednesday', $availables_days, $officeName, $calenderStartDate, $events);
                    break;
                case 'Thu':
                    $events = CalenderService::generateEventDay('thursday', $availables_days, $officeName, $calenderStartDate,  $events);
                    break;
                case 'Fri':
                    $events = CalenderService::generateEventDay('friday', $availables_days, $officeName, $calenderStartDate, $events);
                    break;
                case 'Sat':
                    $events = CalenderService::generateEventDay('saturday', $availables_days, $officeName, $calenderStartDate, $events);
                    break;
                default:
                    break;
            }

            $calenderStartDate->modify('+1 day');
        
        }

        return $events;
    }

    public function filter(array $events, $bookings): array {
        $toReturn = [];
        $toDeletes = [];
        foreach($events as $key => $event) {
            $startEvent = new DateTime($event["start"]);
            $endEvent = new DateTime($event["end"]);
            foreach($bookings as $booking) {
                if ($booking->getStartDate() == $startEvent && $booking->getEndDate() == $endEvent) {
                    array_push($toDeletes, $key);
                    
                }
            }
        }

        if(empty($toDeletes)) return $events;

        foreach($events as $keyEvent => $event) {
            foreach($toDeletes as $key) {
                if ($keyEvent != $key) {
                    array_push($toReturn, $event);
                }
            }
        }
        


        return $toReturn;
    }

    public static function generateEventDay(String $day, $availables_days, $officeName, $calenderStartDate, $events) {
        if (in_array($day.'_morning', $availables_days)) {
            array_push($events, CalenderService::generateEvent( $officeName . ' morning booking', $calenderStartDate, 9, 12));
        } 
        if (in_array($day.'_afternoon', $availables_days)) {
            array_push($events, CalenderService::generateEvent($officeName . 'afternoon booking', $calenderStartDate, 13, 18));
        }


        return $events;
    }

    public static function generateEvent(String $title, DateTime $calenderStartDate, int $hourStart, int $hourEnd) {
        $startDate = $calenderStartDate->setTime($hourStart, 0)->format('Y-m-d\TH:i:s');
        $endDate = $calenderStartDate->setTime($hourEnd, 0)->format('Y-m-d\TH:i:s'); 
        return ['title'=> $title,'start'=> $startDate, 'end'=>$endDate ];
    }
}