<?php

namespace App\Entity;

use App\Repository\CaracteristicsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: CaracteristicsRepository::class)]
class Caracteristics
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups('classicalUser')]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups('classicalUser')]
    private ?string $libelle = null;

    #[ORM\Column(length: 255)]
    #[Groups('classicalUser')]
    private ?string $type = null;

    #[ORM\Column(type: Types::SMALLINT)]
    #[Groups('classicalUser')]
    private ?int $multiple = null;

    #[ORM\ManyToMany(targetEntity: Office::class, inversedBy: 'caracteristics')]
    private Collection $office;

    public function __construct()
    {
        $this->office = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getMultiple(): ?int
    {
        return $this->multiple;
    }

    public function setMultiple(int $multiple): self
    {
        $this->multiple = $multiple;

        return $this;
    }

    /**
     * @return Collection<int, Office>
     */
    public function getOffice(): Collection
    {
        return $this->office;
    }

    public function addOffice(Office $office): self
    {
        if (!$this->office->contains($office)) {
            $this->office->add($office);
        }

        return $this;
    }

    public function removeOffice(Office $office): self
    {
        $this->office->removeElement($office);

        return $this;
    }
}
