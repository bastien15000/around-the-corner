<?php

namespace App\Entity;

use App\Repository\OfficeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: OfficeRepository::class)]
class Office
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups('classicalUser')]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups('classicalUser')]
    private ?string $name = null;

    #[ORM\Column(type: Types::SMALLINT)]
    #[Groups('classicalUser')]
    private ?int $valid = null;

    #[ORM\OneToMany(mappedBy: 'office', targetEntity: Notice::class)]
    #[Groups('classicalUser')]
    private Collection $notices;

    #[ORM\ManyToMany(targetEntity: Caracteristics::class, mappedBy: 'office')]
    #[Groups('classicalUser')]
    private Collection $caracteristics;

    #[ORM\Column(length: 255)]
    #[Groups('classicalUser')]
    private ?string $adress = null;

    #[ORM\Column(length: 255)]
    #[Groups('classicalUser')]
    private ?string $size = null;

    #[ORM\Column(nullable: true)]
    #[Groups('classicalUser')]
    private array $availables_day = [];

    #[ORM\ManyToOne(inversedBy: 'offices')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $creator = null;

    #[ORM\Column(nullable: true)]
    #[Groups('classicalUser')]
    private ?int $capacity = null;

    #[ORM\Column(nullable: true)]
    #[Groups('classicalUser')]
    private ?float $price = null;

    #[ORM\OneToMany(mappedBy: 'office', targetEntity: Booking::class, orphanRemoval: true)]
    #[Groups('classicalUser')]
    private Collection $bookings;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups('classicalUser')]
    private ?string $description = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $latitude = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $longitude = null;

    #[ORM\Column(length: 2000, nullable: true)]
    private ?string $picture_url = null;

    #[Groups('classicalUser')]
    #[ORM\Column(length: 255, nullable: true)]
    private ?string $city = null;

    public function __construct()
    {
        $this->bookings = new ArrayCollection();
        $this->notices = new ArrayCollection();
        $this->caracteristics = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getValid(): ?int
    {
        return $this->valid;
    }

    public function setValid(int $valid): self
    {
        $this->valid = $valid;

        return $this;
    }

    /**
     * @return Collection<int, Notice>
     */
    public function getNotices(): Collection
    {
        return $this->notices;
    }

    public function addNotice(Notice $notice): self
    {
        if (!$this->notices->contains($notice)) {
            $this->notices->add($notice);
            $notice->setOffice($this);
        }

        return $this;
    }

    public function removeNotice(Notice $notice): self
    {
        if ($this->notices->removeElement($notice)) {
            // set the owning side to null (unless already changed)
            if ($notice->getOffice() === $this) {
                $notice->setOffice(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection<int, Caracteristics>
     */
    public function getCaracteristics(): Collection
    {
        return $this->caracteristics;
    }

    public function addCaracteristic(Caracteristics $caracteristic): self
    {
        if (!$this->caracteristics->contains($caracteristic)) {
            $this->caracteristics->add($caracteristic);
            $caracteristic->addOffice($this);
        }

        return $this;
    }

    public function setCaracteristics(ArrayCollection $caracteristics): self
    {
        $this->caracteristics = $caracteristics;

        return $this;
    }

    public function removeCaracteristic(Caracteristics $caracteristic): self
    {
        if ($this->caracteristics->removeElement($caracteristic)) {
            $caracteristic->removeOffice($this);
        }

        return $this;
    }

    public function removeCaracteristics(): self
    {
        foreach ($this->caracteristics as $caracteristic) {
            if ($this->caracteristics->removeElement($caracteristic)) {
                $caracteristic->removeOffice($this);
            }
        }

        return $this;
    }

    public function getAdress(): ?string
    {
        return $this->adress;
    }

    public function setAdress(string $adress): self
    {
        $this->adress = $adress;

        return $this;
    }

    public function getSize(): ?string
    {
        return $this->size;
    }

    public function setSize(string $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getAvailablesDay(): array
    {
        return $this->availables_day;
    }

    public function setAvailablesDay(?array $availables_day): self
    {
        $this->availables_day = $availables_day;

        return $this;
    }

    public function getCreator(): ?User
    {
        return $this->creator;
    }

    public function setCreator(?User $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    public function setCapacity(?int $capacity): self
    {
        $this->capacity = $capacity;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return Collection<int, Booking>
     */
    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    public function addBooking(Booking $booking): self
    {
        if (!$this->bookings->contains($booking)) {
            $this->bookings->add($booking);
            $booking->setOffice($this);
        }

        return $this;
    }

    public function removeBooking(Booking $booking): self
    {
        if ($this->bookings->removeElement($booking)) {
            // set the owning side to null (unless already changed)
            if ($booking->getOffice() === $this) {
                $booking->setOffice(null);
            }
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(?string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(?string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getPictureUrl(): ?string
    {
        return $this->picture_url;
    }

    public function setPictureUrl(?string $picture_url): self
    {
        $this->picture_url = $picture_url;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }
}
