<?php

namespace App\Repository;

use App\Entity\Caracteristics;
use App\Entity\Office;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Office>
 *
 * @method Office|null find($id, $lockMode = null, $lockVersion = null)
 * @method Office|null findOneBy(array $criteria, array $orderBy = null)
 * @method Office[]    findAll()
 * @method Office[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OfficeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Office::class);
    }

    public function save(Office $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Office $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function search(string $data)
    {
        $data = explode("&", $data);

        foreach ($data as $pair) {
            $keyValue = explode("=", $pair);
            $key = $keyValue[0];
            $value = $keyValue[1] ?? "";

            $params[$key] = trim($value);
        }
        $qb =  $this->createQueryBuilder('o');
        if (!empty($params['search'])) {
            $qb->where('o.name LIKE :val')
                ->setParameter('val', "%" . $params['search'] . "%");
        } else {
            if (isset($params['minimum-price']) && $params['minimum-price'] != '') {
                $qb->andwhere('o.price >= :val')
                    ->setParameter('val', $params['minimum-price']);
            }
            if (isset($params['maximum-price']) && $params['maximum-price'] != '') {
                $qb->andwhere('o.price <= :val')
                    ->setParameter('val', $params['maximum-price']);
            }
            if (isset($params['stars']) && $params['stars'] != '') {
                $qb->andwhere('o.stars <= :val')
                    ->setParameter('val', $params['stars']);
            }
            // if (!empty($params['caracteristics'])) {
            //     $qb->join(Caracteristics::class, 'c')
            //         ->where($qb->expr()->in('c.id', $params['caracteristics']));
            // }
        }
        return $qb->orderBy('o.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    //    /**
    //     * @return Office[] Returns an array of Office objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('o')
    //            ->andWhere('o.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('o.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Office
    //    {
    //        return $this->createQueryBuilder('o')
    //            ->andWhere('o.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
