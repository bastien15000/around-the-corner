<?php

namespace App\DataFixtures;

use App\Entity\Caracteristics;
use DateTime;
use App\Entity\Booking;
use App\Entity\Office;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{

    private $CARACTERISTICS = [
        'Equipé (écran, clavier, ...)',
        'Wifi',
        'Accès à une cuisine',
        'Salle de repos',
        'Connexion éthernet',
        'Présence de résidents',
        'Climatisation',
        'Calme'
    ];

    public function __construct(
        private UserPasswordHasherInterface $userPasswordHasher,
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        // Create admin password hashé => testtest
        $admin = $this->getAdmin();
        $users = $this->createUsers($manager);
        $offices = $this->createOffices($manager, $users);
        $this->createCaracteristics($manager);

         foreach ($offices as $office) {
             $admin->addOffice($office);
         }

        $manager->persist($admin);
        $manager->flush();
    }

    /**
     * @return User
     */
    public function getAdmin(): User
    {
        $admin = new User();
        $admin->setEmail("admin@gmail.com");
        $admin->setFirstName("admin");
        $admin->setLastName("admin");
        $admin->setPassword('$2y$13$HiIFN4rEAwk/TY1b2gMtMOrgZ7b/5wuq.4VTihTVEc8xa9V7Dr6M2');
        $admin->setAddress("0 Avenue du non");
        $admin->setCity("Lyon");
        $admin->setProfilePictureURL("https://storage.googleapis.com/pfpai/styles/2669db6a-44ca-4e89-b5fa-c45044608487.png?X-Goog-Algorithm=GOOG4-RSA-SHA256&X-Goog-Credential=firebase-adminsdk-hu3sa%40stockai-362303.iam.gserviceaccount.com%2F20230328%2Fauto%2Fstorage%2Fgoog4_request&X-Goog-Date=20230328T071705Z&X-Goog-Expires=518400&X-Goog-SignedHeaders=host&X-Goog-Signature=267969ae65c008dc0bfde22eec46fc0442018a606081cca0cdd6df484217d9f71a8a68f7612ae9b8e95b27f8657efa47469cd5a3f07e8349721f6aa84461c26260262b2b74fd58eba5156111ace885b23162bee9a0dd2b912500a0e51c5ffe7a92a365c38697dd19bda6061e2913c1c0da96c6b687ddd83f43b1a455433e5d9185c7228cfc70b736124497367f871c52d7eb99e2f45a33c3dc76bf161010f9c100693d98259fb367151d55369b9dee748606f6234c9c9e4aabf917e0fd4e9e01b69e10eb1361a399c0ba0fcfa79a4ef47af48ff2282fa225476c282bb4f34ab976e50a3c8520f3c054eaff3fcb710bed26b4d45bf51dbd26f7ba1a561cb20399");
        $admin->setRoles(["ROLE_ADMIN"]);
        return $admin;
    }

    /**
     * @param ObjectManager $manager
     * @return array
     */
    public function createUsers(ObjectManager $manager): array
    {
        $users = [];
        for ($i = 0; $i < 5; $i++) {
            $user = new User();
            $user->setEmail("user$i@gmail.com");
            $user->setFirstName("user-$i");
            $user->setLastName("user-$i");
            $user->setAddress("$i Avenue du non");
            $user->setCity("Lyon");
            $user->setProfilePictureURL("https://storage.googleapis.com/pfpai/styles/2669db6a-44ca-4e89-b5fa-c45044608487.png?X-Goog-Algorithm=GOOG4-RSA-SHA256&X-Goog-Credential=firebase-adminsdk-hu3sa%40stockai-362303.iam.gserviceaccount.com%2F20230328%2Fauto%2Fstorage%2Fgoog4_request&X-Goog-Date=20230328T071705Z&X-Goog-Expires=518400&X-Goog-SignedHeaders=host&X-Goog-Signature=267969ae65c008dc0bfde22eec46fc0442018a606081cca0cdd6df484217d9f71a8a68f7612ae9b8e95b27f8657efa47469cd5a3f07e8349721f6aa84461c26260262b2b74fd58eba5156111ace885b23162bee9a0dd2b912500a0e51c5ffe7a92a365c38697dd19bda6061e2913c1c0da96c6b687ddd83f43b1a455433e5d9185c7228cfc70b736124497367f871c52d7eb99e2f45a33c3dc76bf161010f9c100693d98259fb367151d55369b9dee748606f6234c9c9e4aabf917e0fd4e9e01b69e10eb1361a399c0ba0fcfa79a4ef47af48ff2282fa225476c282bb4f34ab976e50a3c8520f3c054eaff3fcb710bed26b4d45bf51dbd26f7ba1a561cb20399");
            $user->setPassword($this->userPasswordHasher->hashPassword(
                $user,
                "user$i"
            ));
            $users[] = $user;
            $manager->persist($user);
        }
        return $users;
    }

    /**
     * @param ObjectManager $manager
     * @param array $users
     * @return array
     */
    public function createOffices(ObjectManager $manager, array $users): array
    {
        for ($i = 0; $i < 5; $i++) {
            $office = new Office();
            $office->setName("Bureau $i");
            $office->setValid(1);
            $office->setAdress("$i Rue des bureaux");
            $office->setSize($i . "0");
            $office->setCapacity(1 + $i);
            $office->setPrice(20 + $i);
            switch ($i) {
                case 0:
                    $office->setLatitude(45.74793798070018);
                    $office->setLongitude(4.8648395326085465);
                    break;
                case 1:
                    $office->setLatitude(45.500498410494146);
                    $office->setLongitude(5.143307380836904);
                    break;
                case 2:
                    $office->setLatitude(46.00365302006012);
                    $office->setLongitude(4.458651805010222);
                    break;
                case 3:
                    $office->setLatitude(46.00005143701748);
                    $office->setLongitude(5.02509731416381);
                    break;
                case 4:
                    $office->setLatitude(45.694208103468796);
                    $office->setLongitude(4.939564951098686);
                    break;
                }
            $office->setPictureUrl("https://media.istockphoto.com/id/1177797403/fr/photo/immeubles-dappartements-modernes-sur-une-journ%C3%A9e-ensoleill%C3%A9e-avec-un-ciel-bleu.jpg?s=612x612&w=0&k=20&c=RYVqzUo-p9hfMYyHIQyicLMgO84SjLZe70j_CNXaAI0=");
            $manager->persist($office);
            $users[$i]->addOffice($office);
            $offices[] = $office;

            $booking = new Booking();
            $booking->setUser($users[$i]);
            $booking->setOffice($office);
            $booking->setState(3);
            $booking->setCreatedDate(new \DateTime());
            $booking->setStartDate(DateTime::createFromFormat('d/m/Y', '0' . $i . '/04/2023'));
            $booking->setEndDate(DateTime::createFromFormat('d/m/Y', '0' . ($i + 3) . '/04/2023'));

            $manager->persist($users[$i]);
            $manager->persist($booking);
        }

        return $offices;
    }

    /**
     * @param ObjectManager $manager
     * @param array $offices
     */
    public function createUserWithBookings(ObjectManager $manager, array $offices): void
    {
        $user = new User();
        $user->setEmail("special@gmail.com");
        $user->setFirstName("Utilisateur réservations");
        $user->setLastName("Utilisateur réservations");
        $user->setPassword('$2y$13$HiIFN4rEAwk/TY1b2gMtMOrgZ7b/5wuq.4VTihTVEc8xa9V7Dr6M2');
        $user->setAddress("0 Avenue du non");
        $user->setCity("Lyon");
        $user->setProfilePictureURL("https://storage.googleapis.com/pfpai/styles/2669db6a-44ca-4e89-b5fa-c45044608487.png?X-Goog-Algorithm=GOOG4-RSA-SHA256&X-Goog-Credential=firebase-adminsdk-hu3sa%40stockai-362303.iam.gserviceaccount.com%2F20230328%2Fauto%2Fstorage%2Fgoog4_request&X-Goog-Date=20230328T071705Z&X-Goog-Expires=518400&X-Goog-SignedHeaders=host&X-Goog-Signature=267969ae65c008dc0bfde22eec46fc0442018a606081cca0cdd6df484217d9f71a8a68f7612ae9b8e95b27f8657efa47469cd5a3f07e8349721f6aa84461c26260262b2b74fd58eba5156111ace885b23162bee9a0dd2b912500a0e51c5ffe7a92a365c38697dd19bda6061e2913c1c0da96c6b687ddd83f43b1a455433e5d9185c7228cfc70b736124497367f871c52d7eb99e2f45a33c3dc76bf161010f9c100693d98259fb367151d55369b9dee748606f6234c9c9e4aabf917e0fd4e9e01b69e10eb1361a399c0ba0fcfa79a4ef47af48ff2282fa225476c282bb4f34ab976e50a3c8520f3c054eaff3fcb710bed26b4d45bf51dbd26f7ba1a561cb20399");

        $booking = new Booking();
        $booking->setCreatedDate(new \DateTime());
        $booking->setUpdatedDate(new \DateTime());

        for ($i = 0; $i < 2; $i++) {
            $booking->setOffice($offices[$i]);
        }

        $user->addBooking($booking);
        $manager->persist($user);
    }

    private function createCaracteristics(ObjectManager $manager): void {


        foreach($this->CARACTERISTICS as $CARACTERISTIC) {

            $caracteristics = new Caracteristics();
            $caracteristics->setLibelle($CARACTERISTIC);
            $caracteristics->setType('boolean');
            $caracteristics->setMultiple(0);

            $manager->persist($caracteristics);
        }
    }
}
