<?php 
namespace App\Model;

class OfficeDTO {

    private String $name = "";

    private String $size = "";

    private String $adress = "";

    private ?int $price = null;
    
    private ?int $valid = null;

    private ?int $capacity = null;

    private array $availables_day = [];

    private ?int $creator_id =  null;

    private array $caracteristics = [];

    private string $description = "";

    public function setName(String $name): self {
        $this->name = $name;
        return $this;
    }

    public function getName(): String {
        return $this->name;
    }

    
    public function setSize(String $size): self {
        $this->size = $size;
        return $this;
    }

    public function getSize(): String {
        return $this->size;
    }

    public function setCapacity(String $capacity): self {
        $this->capacity = $capacity;
        return $this;
    }

    public function getPrice(): ?int {
        return $this->price;
    }

    public function setPrice(int $price): self {
        $this->price = $price;
        return $this;
    }

    public function getCapacity(): ?int {
        return $this->capacity;
    }

    public function setAdress(String $adress): self {
        $this->adress = $adress;
        return $this;
    }

    public function getAdress(): String {
        return $this->adress;
    }

    public function setvalid(int $valid): self {
        $this->valid = $valid;
        return $this;
    }

    public function getValid(): ?int {
        return $this->valid;
    }

    public function setAvailablesDay(array $availables_day): self {
        $this->availables_day = $availables_day;
        return $this;
    }

    public function getAvailablesDay(): array {
        return $this->availables_day;
    }

    public function setCreatorId(int $id): self {
        $this->creator_id = $id;
        return $this;
    }

    public function getCreatorId(): ?int {
        return $this->creator_id;
    }

    public function setCaracteristics(array $caracteristics): self {
        $this->caracteristics = $caracteristics;
        return $this;
    }

    public function getCaracteristics(): array {
        return $this->caracteristics;
    }

    public function setDescription(String $description): self {
        $this->description = $description;
        return $this;
    }

    public function getDescription(): String {
        return $this->description;
    }

}